FROM node:14
WORKDIR /app
ADD ./app /app
EXPOSE 3000
CMD yarn install && yarn run start