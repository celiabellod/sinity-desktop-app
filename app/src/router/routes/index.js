import { lazy } from 'react'
import AppRoutes from './Apps'
import PagesRoutes from './Pages'

// ** Document title
const TemplateTitle = '%s - Vuexy React Admin Template'

// ** Default Route
const DefaultRoute = '/login'

// ** Merge Routes
const Routes = [
  {
    path: '/home',
    component: lazy(() => import('../../views/Home')),
  },
  {
    path: '/error',
    component: lazy(() => import('../../views/Error')),
    layout: 'BlankLayout'
  },
  ...AppRoutes,
  ...PagesRoutes,
]

export { DefaultRoute, TemplateTitle, Routes }
