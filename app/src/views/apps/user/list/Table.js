// ** React Imports
import { Fragment, useState, useEffect, useContext } from "react";

// ** Invoice List Sidebar
import Sidebar from "./Sidebar";

// ** Columns
import { columns } from "./columns";

// ** Store & Actions
import { GetLengthData, GetData } from "../store/action";
import { useDispatch, useSelector } from "react-redux";

// ** Third Party Components
import Select from "react-select";
import ReactPaginate from "react-paginate";
import { ChevronDown } from "react-feather";
import DataTable from "react-data-table-component";
import { selectThemeColors } from "@utils";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  Input,
  Row,
  Col,
  Label,
  CustomInput,
  Button,
} from "reactstrap";

// ** Styles
import "@styles/react/libs/react-select/_react-select.scss";
import "@styles/react/libs/tables/react-dataTable-component.scss";
import axios from "axios";
import userMessagesFr from '@src/assets/data/locales/fr.json'

// ** Table Header
const CustomHeader = ({
  toggleSidebar,
  handlePerPage,
  rowsPerPage,
  handleFilter,
  searchTerm,
}) => {
  return (
    <div className="invoice-list-table-header w-100 mr-1 ml-50 mt-2 mb-75">
      <Row>
        <Col xl="6" className="d-flex align-items-center p-0">
          <div className="d-flex align-items-center w-100">
            <CustomInput
              className="form-control mx-50"
              type="select"
              id="rows-per-page"
              value={rowsPerPage}
              onChange={handlePerPage}
              style={{
                width: 5,
                padding: "0 0.8rem",
                backgroundPosition:
                  "calc(100% - 3px) 11px, calc(100% - 20px) 13px, 100% 0",
              }}
            >
              <option value="10">10</option>
              <option value="25">25</option>
              <option value="50">50</option>
            </CustomInput>
          </div>
        </Col>
        <Col
          xl="6"
          className="d-flex align-items-sm-center justify-content-lg-end justify-content-start flex-lg-nowrap flex-wrap flex-sm-row flex-column pr-lg-1 p-0 mt-lg-0 mt-1"
        >
          <Button.Ripple color="primary" onClick={toggleSidebar}>
            {(userMessagesFr["Add user"] != undefined) ? userMessagesFr["Add user"] : "Add user"}
          </Button.Ripple>
        </Col>
      </Row>
    </div>
  );
};

const UsersList = () => {
  // ** Store Vars
  const dispatch = useDispatch();
  const store = useSelector((state) => state.users);
  const authStore = useSelector(state => state.auth)

  // ** States
  const [searchTerm, setSearchTerm] = useState("");
  const [currentPage, setCurrentPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);
  const [sidebarOpen, setSidebarOpen] = useState(false);
  const [currentGender, setCurrentGender] = useState({
    value: 0,
    label: (userMessagesFr["Choose a sex"] != undefined) ? userMessagesFr["Choose a sex"] : "Choose a sex",
  });
  const [currentAstroSign, setCurrentAstroSign] = useState({
    value: 0,
    label: (userMessagesFr["Choose an astro sign"] != undefined) ? userMessagesFr["Choose an astro sign"] : "Choose an astro sign",
  });
  const [currentCity, setCurrentCity] = useState({
    value: 0,
    label: (userMessagesFr["Choose a city"] != undefined) ? userMessagesFr["Choose a city"] : "Choose a city",
  });
  const [genderOptions, setGenderOptions] = useState([]);
  const [astroSignOptions, setAstroSignOptions] = useState([]);
  const [citiesOptions, setCitiesOptions] = useState([]);

  const setSkip = (currentPage, rowsPerPage) => {
    return currentPage == 0 ? 0 : currentPage * rowsPerPage;
  };

  const setLimit = (currentPage, rowsPerPage) => {
    return currentPage == 0
      ? rowsPerPage
      : currentPage * (rowsPerPage + rowsPerPage);
  };

  // ** Function to toggle sidebar
  const toggleSidebar = () => setSidebarOpen(!sidebarOpen);

  const datas = store.data == undefined ? "" : store.data.length;

  // ** Get data on mount
  useEffect(() => {
    dispatch(GetLengthData(authStore));
    dispatch(
      GetData({
        skip: setSkip(currentPage, rowsPerPage),
        limit: setLimit(currentPage, rowsPerPage),
        gender_id: currentGender.value,
        astro_sign_id: currentAstroSign.value,
        city_id: currentCity.value,
        q: searchTerm,
      }, authStore)
    );
  }, [dispatch]);

  useEffect(() => {
    CustomPagination();
  }, [store.data]);
  // ** User filter options

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_ENDPOINT + "genders", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => {
        res.data.splice(0, 0,{
          id: 0,
          name: (userMessagesFr["Choose a sex"] != undefined) ? userMessagesFr["Choose a sex"] : "Choose a sex",
        });

        setGenderOptions(
          res.data.map((item) => {
            return {
              value: item.id,
              label: (userMessagesFr[item.name] != undefined) ? userMessagesFr[item.name] : item.name,
            };
          })
        );
      });

    axios
      .get(process.env.REACT_APP_API_ENDPOINT + "astroSigns", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
          "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => {
        res.data.splice(0, 0,{
          id: 0,
          name: (userMessagesFr["Choose and astro sign"] != undefined) ? userMessagesFr["Choose and astro sign"] : "Choose and astro sign",
        });

        setAstroSignOptions(
          res.data.map((item) => {
            return {
              value: item.id,
              label: (userMessagesFr[item.name] != undefined) ? userMessagesFr[item.name] : item.name,
            };
          })
        );
      });

    axios
      .get(process.env.REACT_APP_API_ENDPOINT + "cities", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
          "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => {
        res.data.splice(0, 0,{
          id: 0,
          name: (userMessagesFr["Choose a city"] != undefined) ? userMessagesFr["Choose a city"] : "Choose a city",
        });

        setCitiesOptions(
          res.data.map((item) => {
            return {
              value: item.id,
              label:  (userMessagesFr[item.name] != undefined) ? userMessagesFr[item.name] : item.name,
            };
          })
        );
      });
  }, []);

  // ** Function in get data on page change
  const handlePagination = (page) => {
    dispatch(
      GetData({
        skip: setSkip(page.selected, rowsPerPage),
        limit: setLimit(page.selected, rowsPerPage),
        gender_id: currentGender.value,
        astro_sign_id: currentAstroSign.value,
        city_id: currentCity.value,
        q: searchTerm,
      }, authStore)
    );
    setCurrentPage(page.selected + 1);
  };

  // ** Function in get data on rows per page
  const handlePerPage = (e) => {
    const value = parseInt(e.currentTarget.value);
    setCurrentPage(0);
    dispatch(
      GetData({
        skip: setSkip(0, value),
        limit: setLimit(0, value),
        gender_id: currentGender.value,
        astro_sign_id: currentAstroSign.value,
        city_id: currentCity.value,
        q: searchTerm,
      }, authStore)
    );
    setRowsPerPage(value);
  };

  // ** Function in get data on search query change
  const handleFilter = (val) => {
    setSearchTerm(val);
    dispatch(
      GetData({
        skip: setSkip(currentPage, rowsPerPage),
        limit: setLimit(currentPage, rowsPerPage),
        gender_id: currentGender.value,
        astro_sign_id: currentAstroSign.value,
        city_id: currentCity.value,
        q: val,
      }, authStore)
    );
  };

  // ** Custom Pagination
  const CustomPagination = () => {
    const datasLength =
      currentGender.value != 0 ||
      currentAstroSign.value != 0 ||
      currentCity.value != 0
        ? store.data.length
        : store.lengthData;

    const count = Number(Math.ceil(datasLength / rowsPerPage));

    return (
      <ReactPaginate
        previousLabel={""}
        nextLabel={""}
        pageCount={count || 1}
        activeClassName="active"
        forcePage={currentPage !== 0 ? currentPage - 1 : 0}
        onPageChange={(page) => handlePagination(page)}
        pageClassName={"page-item"}
        nextLinkClassName={"page-link"}
        nextClassName={"page-item next"}
        previousClassName={"page-item prev"}
        previousLinkClassName={"page-link"}
        pageLinkClassName={"page-link"}
        containerClassName={
          "pagination react-paginate justify-content-end my-2 pr-1"
        }
      />
    );
  };

  // ** Table data to render
  const dataToRender = () => {
    const filters = {
      gender: currentGender.value,
      astroSign: currentAstroSign.value,
      cities: currentCity.value,
      q: searchTerm,
    };

    const isFiltered = Object.keys(filters).some(function (k) {
      return filters[k].length > 0;
    });

    if (store.data != undefined && store.data.length === 0 && isFiltered) {
      return [];
    } else {
      return store.data;
    }
  };

  return (
    <Fragment>
      <Card>
        <CardHeader>
          <CardTitle tag="h4">Filtre de recherche</CardTitle>
        </CardHeader>
        <CardBody>
          <Row>
            <Col md="4">
              <Select
                isClearable={false}
                theme={selectThemeColors}
                className="react-select"
                classNamePrefix="select"
                options={genderOptions}
                value={currentGender}
                onChange={(data) => {
                  setCurrentGender(data);
                  dispatch(
                    GetData({
                      skip: currentPage,
                      limit: rowsPerPage,
                      gender_id: data.value,
                      astro_sign_id: currentAstroSign.value,
                      city_id: currentCity.value,
                      q: searchTerm,
                    }, authStore)
                  );
                }}
              />
            </Col>
            <Col className="my-md-0 my-1" md="4">
              <Select
                theme={selectThemeColors}
                isClearable={false}
                className="react-select"
                classNamePrefix="select"
                options={astroSignOptions}
                value={currentAstroSign}
                onChange={(data) => {
                  setCurrentAstroSign(data);
                  dispatch(
                    GetData({
                      skip: currentPage,
                      limit: rowsPerPage,
                      gendr_id: currentGender.value,
                      astro_sign_id: data.value,
                      city_id: currentCity.value,
                      q: searchTerm,
                    }, authStore)
                  );
                }}
              />
            </Col>
            <Col md="4">
              <Select
                theme={selectThemeColors}
                isClearable={false}
                className="react-select"
                classNamePrefix="select"
                options={citiesOptions}
                value={currentCity}
                onChange={(data) => {
                  setCurrentCity(data);
                  dispatch(
                    GetData({
                      skip: currentPage,
                      limit: rowsPerPage,
                      gender_id: currentGender.value,
                      astro_sign_id: currentAstroSign.value,
                      city_id: data.value,
                      q: searchTerm,
                    }, authStore)
                  );
                }}
              />
            </Col>
          </Row>
          <Row>
            <Col md="12 mx-0 mt-2 mb-0">
              <div className="d-flex align-items-center">
                <Input
                  id="search-invoice"
                  className="w-100"
                  type="text"
                  value={searchTerm}
                  onChange={(e) => handleFilter(e.target.value)}
                  placeholder="Rechercher"
                />
              </div>
            </Col>
          </Row>
        </CardBody>
      </Card>

      <Card>
        <DataTable
          noHeader
          pagination
          subHeader
          responsive
          paginationServer
          columns={columns}
          sortIcon={<ChevronDown />}
          className="react-dataTable"
          paginationComponent={CustomPagination}
          data={dataToRender()}
          subHeaderComponent={
            <CustomHeader
              toggleSidebar={toggleSidebar}
              handlePerPage={handlePerPage}
              rowsPerPage={rowsPerPage}
              searchTerm={searchTerm}
              handleFilter={handleFilter}
            />
          }
        />
      </Card>

      <Sidebar open={sidebarOpen} toggleSidebar={toggleSidebar} />
    </Fragment>
  );
};

export default UsersList;
