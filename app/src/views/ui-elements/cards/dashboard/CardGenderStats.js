import Chart from "react-apexcharts";
import { MoreVertical } from "react-feather";
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  CardText,
  Media,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
} from "reactstrap";

import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import axios from "axios";

const CardGenderStats = ({ colors, trackBgColor }) => {

  const [femaleNumber, setFemaleNumber] = useState(null);
  const [maleNumber, setMaleNumber] = useState(null);
  const authStore = useSelector(state => state.auth)
  const male = 1;
  const female = 2;

  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_ENDPOINT + "users/gender/" + female, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => setFemaleNumber(res.data.length));

    axios
      .get(process.env.REACT_APP_API_ENDPOINT + "users/gender/" + male, {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => setMaleNumber(res.data.length));
  }, []);

  const pourcentage = (number) => {
    return Math.trunc(number*100/(femaleNumber+maleNumber));
  };

  const statesArr = [
    {
      avatar: require("@src/assets/images/icons/female.png").default,
      title: "Femme",
      value: (femaleNumber != undefined) ? pourcentage(femaleNumber) + '%' : '0%',
      chart: {
        type: "radialBar",
        series: [54.4],
        height: 30,
        width: 30,
        options: {
          grid: {
            show: false,
            padding: {
              left: -15,
              right: -15,
              top: -12,
              bottom: -15,
            },
          },
          colors: [colors.primary.main],
          plotOptions: {
            radialBar: {
              hollow: {
                size: "22%",
              },
              track: {
                background: trackBgColor,
              },
              dataLabels: {
                showOn: "always",
                name: {
                  show: false,
                },
                value: {
                  show: false,
                },
              },
            },
          },
          stroke: {
            lineCap: "round",
          },
        },
      },
    },
    {
      avatar: require("@src/assets/images/icons/male.png").default,
      title: "Homme",
      value: (maleNumber != undefined) ? pourcentage(maleNumber) + '%' : '0%',
      chart: {
        type: "radialBar",
        series: [6.1],
        height: 30,
        width: 30,
        options: {
          grid: {
            show: false,
            padding: {
              left: -15,
              right: -15,
              top: -12,
              bottom: -15,
            },
          },
          colors: [colors.warning.main],
          plotOptions: {
            radialBar: {
              hollow: {
                size: "22%",
              },
              track: {
                background: trackBgColor,
              },
              dataLabels: {
                showOn: "always",
                name: {
                  show: false,
                },
                value: {
                  show: false,
                },
              },
            },
          },
          stroke: {
            lineCap: "round",
          },
        },
      },
    },
  ];

  const renderStates = () => {
    return statesArr.map((state) => {
      return (
        <div key={state.title} className="browser-states">
          <Media>
            <img
              className="rounded mr-1"
              src={state.avatar}
              height="30"
              alt={state.title}
            />
            <h6 className="align-self-center mb-0">{state.title}</h6>
          </Media>
          <div className="d-flex align-items-center">
            <div className="font-weight-bold text-body-heading mr-1">
              {state.value}
            </div>
            <Chart
              options={state.chart.options}
              series={state.chart.series}
              type={state.chart.type}
              height={state.chart.height}
              width={state.chart.width}
            />
          </div>
        </div>
      );
    });
  };

  return (
    <Card className="card-browser-states">
      <CardHeader>
        <div>
          <CardTitle tag="h4">Sexe</CardTitle>
        </div>
      </CardHeader>
      <CardBody>{renderStates()}</CardBody>
    </Card>
  );
};

export default CardGenderStats;
