import { useContext } from "react";
import { useSelector } from "react-redux";
import { ThemeColors } from "@src/utility/context/ThemeColors";
import CardGenderState from "@src/views/ui-elements/cards/dashboard/CardGenderStats";
import {
  Row,
  Col,
} from "reactstrap";
import StatsVertical from "@components/widgets/stats/StatsVertical";
import StatsHorizontal from "@components/widgets/stats/StatsHorizontal";
import { Users, Activity } from "react-feather";
import { useEffect, useState } from "react";
import axios from "axios";

import "@styles/react/libs/charts/apex-charts.scss";


const Home = () => {
  const { colors } = useContext(ThemeColors);
  const [data, setData] = useState(null);
  const [matches, setMatches] = useState(null);
  const authStore = useSelector(state => state.auth);
  
  useEffect(() => {
    axios
      .get(process.env.REACT_APP_API_ENDPOINT + "users", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => setData(res.data));

      axios
      .get(process.env.REACT_APP_API_ENDPOINT + "matches", {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
            "Bearer " + authStore.userData.accessToken,
        },
      })
      .then((res) => setMatches(res.data));

  }, []);

  return (
    <div id="dashboard-analytics">
      <Row className="match-height">
        <Col xl="2" md="4" sm="6">
          <StatsVertical
            icon={<Users size={21} />}
            color="info"
            stats={data ? data.length.toString() : ""}
            statTitle="Utilisateurs"
          />
        </Col>

        <Col lg="3" sm="6">
          <StatsHorizontal
            icon={<Activity size={21} />}
            color="success"
            stats={matches ? matches.length.toString()  : ""}
            statTitle="Matches"
          />
        </Col>
        <Col lg="3" sm="6">
          <CardGenderState colors={colors}/>
        </Col>
      </Row>
    </div>
  );
};

export default Home;
