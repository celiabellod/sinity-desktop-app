// ** UseJWT import to get config
import useJwt from "@src/@core/auth/jwt/useJwt";

// ** Handle User Login
export const HandleLogin = (data) => {
  const config = useJwt({
    storageTokenKeyName: "access_token",
    storageRefreshTokenKeyName: "refresh_token",
  });

  return (dispatch) => {
    dispatch({
      type: "LOGIN",
      data,
      config,
      [config.storageTokenKeyName]: data[config.storageTokenKeyName],
      [config.storageRefreshTokenKeyName]:
        data[config.storageRefreshTokenKeyName],
    });


    // ** Add to user, accessToken & refreshToken to localStorage
    localStorage.setItem("userData", JSON.stringify(data));
    localStorage.setItem(
      config.storageTokenKeyName,
      JSON.stringify(data.accessToken)
    );
    localStorage.setItem(
      config.storageRefreshTokenKeyName,
      JSON.stringify(data.refreshToken)
    );
  };
};

// ** Handle User Logout
export const HandleLogout = () => {
  const config = useJwt({
    storageTokenKeyName: "access_token",
    storageRefreshTokenKeyName: "refresh_token",
  });
  return (dispatch) => {
    dispatch({
      type: "LOGOUT",
      [config.storageTokenKeyName]: null,
      [config.storageRefreshTokenKeyName]: null,
    });

    // ** Remove user, accessToken & refreshToken from localStorage
    localStorage.removeItem("userData");
    localStorage.removeItem(config.storageTokenKeyName);
    localStorage.removeItem(config.storageRefreshTokenKeyName);
  };
};
